_PLUGINS = []

data = {
    "name": "mode choice",
    "desc": "handle mode choice menu",
    "SHOW_IN_LIST": 0
}

class STATE_choose_mode:
    mode = "modechoice"
    action = "mode_choice"
    def run(self, data):
        self.factory.users[self.ID]["mode"] = data["choice"]
        self.send_json({
            "mode": data["choice"],
            "errorcode": 0
        })

functions = [STATE_choose_mode,]
