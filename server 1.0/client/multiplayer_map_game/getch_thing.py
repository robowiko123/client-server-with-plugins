import os
if os.name == 'nt':
    import msvcrt

    import ctypes

    kernel32 = ctypes.windll.kernel32
    kernel32.SetConsoleMode(kernel32.GetStdHandle(-11), 7)
    
    class Window():
        K_UP    = 0
        K_DOWN  = 1
        K_LEFT  = 3
        K_RIGHT = 4
        def __init__(self, x, y, w, h):
            self.buffer = [[["", " "] for i in range(w)] for _ in range(h)]
            self.x = x
            self.y = y
            self.w = w
            self.h = h
            self.focused = False
        def refresh(self):
            y = 0
            for row in self.buffer:
                x = 0
                for cell in row:
                    print("\033[%d;%dH%s" % (x, y, cell[0] + cell[1]), end="", flush=True)
                    
                    x += 1
                y += 1
        def get_input(self):
            if kbhit():
                if self.focused:
                    return getch()
            return ""
        def addstr(self, x, y, s):
            z = 0
            for c in s:
                self.buffer[y][x + z] = [self.buffer[y][x + z][0], c]
                print(y, x + z, [self.buffer[y][x + z][0], c])
                z += 1
        def change_color(self, x, y, fg, bg):
            self.buffer[y][x][0] = "\033[%d;%dM" % (fg, bg)

    def getch():
        return msvcrt.getch()
    def kbhit():
        return msvcrt.kbhit()
    def getche():
        return msvcrt.getche()
    def getwch():
        return msvcrt.getwch()
    def getwche():
        return msvcrt.getwche()
    def putch(x):
        return msvcrt.putch(x)
    def putwch(x):
        return msvcrt.putwch(x)
else:
    raise Exception("linux not supported yet")
    import sys, termios, atexit
    from select import select

    # save the terminal settings
    fd = sys.stdin.fileno()
    new_term = termios.tcgetattr(fd)
    old_term = termios.tcgetattr(fd)

    # new terminal setting unbuffered
    new_term[3] = (new_term[3] & ~termios.ICANON & ~termios.ECHO)

    # switch to normal terminal
    def set_normal_term():
        termios.tcsetattr(fd, termios.TCSAFLUSH, old_term)

    # switch to unbuffered terminal
    def set_curses_term():
        termios.tcsetattr(fd, termios.TCSAFLUSH, new_term)

    def putch(ch):
        sys.stdout.write(ch)

    def getch():
        return sys.stdin.read(1)

    def getche():
        ch = getch()
        putch(ch)
        return ch

    def kbhit():
        dr,dw,de = select([sys.stdin], [], [], 0)
        return dr != []

    def getwch():
        return getch().decode("Latin-1")

    atexit.register(set_normal_term)
    set_curses_term()
