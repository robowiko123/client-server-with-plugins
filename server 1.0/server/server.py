import copy
import json
import hashlib
import plugins
from twisted.internet import reactor, protocol
from twisted.internet.protocol import Protocol, Factory
from twisted.internet.endpoints import TCP4ServerEndpoint

UNKNOWN_ACTION         = 1
MALFORMED_QUERY        = 2
WRONG_PASSWD           = 3
ACCOUNT_ALREADY_EXISTS = 4
ACCOUNT_DOESNT_EXIST   = 5

_PLUGINS = plugins.plugins

class Server(Protocol):

    def connectionMade(self):
        self._peer = self.transport.getPeer()
        self.DATA = b""
        self.PLUGIN_INSTANCES = []

        for i in range(len(self.factory.users)):
            self.ID = i
            if self.factory.users[i] == None:
                break
        if len(self.factory.users) == 0:
            self.factory.users.append({
                "ip": self._peer.host,
                "port": self._peer.port,
                "mode": "login",
                "user": "",
                "x": 0,
                "y": 0,
            })
            self.ID = 0
        elif self.factory.users[self.ID] == None:
            self.factory.users[self.ID] = {
                "ip": self._peer.host,
                "port": self._peer.port,
                "mode": "login",
                "user": "",
            }
        else:
            self.factory.users.append({
                "ip": self._peer.host,
                "port": self._peer.port,
                "mode": "login",
                "user": "",
            })
            self.ID += 1
        self.send_json({
            "mode": "login",
            "errorcode": 0
        })
        print(self.factory.users)
        
        x = open("accounts.json", "w")
        json.dump(self.factory.accounts, x)
        x.close()
        
        x = open("serv_data.json", "w")
        json.dump(self.factory.serv_data, x)
        x.close()

    def connectionLost(self, reason):
        self.factory.users[self.ID] = None
        print(self.factory.users)
    
    def dataReceived(self, data_raw):
        self.DATA += data_raw
        if len(self.DATA) >= 1024:
            actual_data = self.DATA[:2048]
            self.DATA = self.DATA[2048:]
        else:
            return
        data = self.load_json(actual_data)

        """if data["metaaction"] == "create_plugin":
            for plugin in _PLUGINS:
                if plugin.data["name"] == data["plugin_name"]:
                    self.PLUGIN_INSTANCES.append({"functions": copy.deepcopy(plugin.functions), "data_buf": []})
                    self.send_json({"result": "ok", "PID": len(self.PLUGIN_INSTANCES)})
                    return
        elif data["metaaction"] == "signal_plugin":
            if data["PID"] < len(self.PLUGIN_INSTANCES):
                self.PLUGIN_INSTANCES[data["PID"]]["data_buf"].append(data["data"])"""
        
        
        #print(data["action"], self.factory.users[self.ID]["mode"])
        for plugin in _PLUGINS:
            for function in plugin.functions:
                #print(function.__name__, function.mode, function.action)
                if function.mode == self.factory.users[self.ID]["mode"]:
                    #print("modes match")
                    if function.action == data["action"]:
                        #print("action match")
                        function.run(self, data)
                        return
        self.send_json({
            "mode": self.factory.users[self.ID]["mode"],
            "errorcode": UNKNOWN_ACTION
        })

    ##################################################################################
    def send_json(self, dict_):
        self.transport.write(json.dumps(dict_).encode("Latin-1").ljust(2048, b"\x01"))
    
    def load_json(self, raw):
        data = raw.split(b"\x01")
        # print(data)
        d2 = []
        for i in data:
            if i != b"":
                d2.append(i)
        if len(d2) > 1:
            d2 = [d2[0], ]
        try:
            return json.loads(d2[0].decode("Latin-1"))
        except:
            print(data)
            raise

class ServerFactory(Factory):

    protocol = Server

    def __init__(self):
        self.users = []
        x = open("accounts.json")
        self.accounts = json.load(x)
        x.close()
        
        x = open("serv_data.json")
        self.serv_data = json.load(x)
        x.close()
        

def main():
    endpoint = TCP4ServerEndpoint(reactor, 35852)
    endpoint.listen(ServerFactory())
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
