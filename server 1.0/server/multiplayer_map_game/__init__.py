import __main__
import json

COLLIDABLE_TILES = ["#", ]

data = {
    "name": "multiplayer_map_game",
    "desc": "A simple multiplayer game",
    "SHOW_IN_LIST": 1
}

ticks_before_autosave = 30

class STATE_map_game_ACTION_move:
    mode = "multiplayer_map_game"
    action = "move"
    def run(self, data):
        current_account = self.factory.accounts[self.factory.users[self.ID]["user"]]
        current_data = current_account["client_data"]["multiplayer_game"]
        game_data = self.factory.serv_data["multiplayer_game"]
        
        if data["direction"] == "UP":
            if (current_data["posY"] - 1) >= 0:
                if game_data["map"][current_data["posY"] - 1][current_data["posX"]] not in COLLIDABLE_TILES:
                    current_data["posY"] -= 1
        elif data["direction"] == "DOWN":
            if (current_data["posY"] + 1) < len(game_data["map"]):
                if game_data["map"][current_data["posY"] + 1][current_data["posX"]] not in COLLIDABLE_TILES:
                    current_data["posY"] += 1
        elif data["direction"] == "LEFT":
            if (current_data["posX"] - 1) >= 0:
                if game_data["map"][current_data["posY"]][current_data["posX"] - 1] not in COLLIDABLE_TILES:
                    current_data["posX"] -= 1
        elif data["direction"] == "RIGHT":
            if (current_data["posX"] + 1) < len(game_data["map"][0]):
                if game_data["map"][current_data["posY"]][current_data["posX"] + 1] not in COLLIDABLE_TILES:
                    current_data["posX"] += 1
        else:
            self.send_json({
                "errorcode": __main__.MALFORMED_QUERY,
                "mode": "multiplayer_map_game",
            })
            return

        self.factory.serv_data["multiplayer_game"] = game_data
        current_account["client_data"]["multiplayer_game"] = current_data
        self.factory.accounts[self.factory.users[self.ID]["user"]] = current_account
        
        self.send_json({
            "map": self.factory.serv_data["multiplayer_game"]["map"],
            "players": {i: self.factory.accounts[i]["client_data"].get("multiplayer_game") for i in self.factory.accounts},
            "errorcode": 0,
            "mode": "multiplayer_map_game",
            "current_account": self.factory.users[self.ID]["user"]
        })
        autosave(self)

class STATE_map_game_ACTION_place:
    mode = "multiplayer_map_game"
    action = "place"
    def run(self, data):
        current_account = self.factory.accounts[self.factory.users[self.ID]["user"]]
        current_data = current_account["client_data"]["multiplayer_game"]
        game_data = self.factory.serv_data["multiplayer_game"]
        
        if current_data["inventory"][data["index"]][1] > 0:
            map = [list(i) for i in game_data["map"]]
            done = 0
            if data["direction"] == "UP":
                if (current_data["posY"] - 1) >= 0:
                    if game_data["map"][current_data["posY"] - 1][current_data["posX"]] == " ":
                        map[current_data["posY"] - 1][current_data["posX"]] = current_data["inventory"][data["index"]][0]
                        done = 1
            elif data["direction"] == "DOWN":
                if (current_data["posY"] + 1) < len(game_data["map"]):
                    if game_data["map"][current_data["posY"] + 1][current_data["posX"]] == " ":
                        
                        map[current_data["posY"] + 1][current_data["posX"]] = current_data["inventory"][data["index"]][0]
                        
                        done = 1
            elif data["direction"] == "LEFT":
                if (current_data["posX"] - 1) >= 0:
                    if game_data["map"][current_data["posY"]][current_data["posX"] - 1] == " ":
                        map[current_data["posY"]][current_data["posX"] - 1] = current_data["inventory"][data["index"]][0]
                        done = 1
            elif data["direction"] == "RIGHT":
                if (current_data["posX"] + 1) < len(game_data["map"][0]):
                    if game_data["map"][current_data["posY"]][current_data["posX"] + 1] == " ":
                        map[current_data["posY"]][current_data["posX"] + 1] = current_data["inventory"][data["index"]][0]
                        done = 1
            else:
                self.send_json({
                    "errorcode": __main__.MALFORMED_QUERY,
                    "mode": "multiplayer_map_game",
                })
                return
            game_data["map"] = ["".join(i) for i in map]
            if done:
                current_data["inventory"][data["index"]][1] -= 1

        self.factory.serv_data["multiplayer_game"] = game_data
        current_account["client_data"]["multiplayer_game"] = current_data
        self.factory.accounts[self.factory.users[self.ID]["user"]] = current_account
        
        self.send_json({
            "map": self.factory.serv_data["multiplayer_game"]["map"],
            "players": {i: self.factory.accounts[i]["client_data"].get("multiplayer_game") for i in self.factory.accounts},
            "errorcode": 0,
            "mode": "multiplayer_map_game",
            "current_account": self.factory.users[self.ID]["user"]
        })
        autosave(self)

class STATE_map_game_ACTION_destroy:
    mode = "multiplayer_map_game"
    action = "destroy"
    def run(self, data):
        current_account = self.factory.accounts[self.factory.users[self.ID]["user"]]
        current_data = current_account["client_data"]["multiplayer_game"]
        game_data = self.factory.serv_data["multiplayer_game"]
        
        if 1: # current_data["inventory"][data["index"]][1] > 0:
            map = [list(i) for i in game_data["map"]]
            
            if data["direction"] == "UP":
                if (current_data["posY"] - 1) >= 0:
                    if game_data["map"][current_data["posY"] - 1][current_data["posX"]] != " ":
                        done = 0
                        for i in current_data["inventory"]:
                            if i[0] == map[current_data["posY"] - 1][current_data["posX"]]:
                                i[1] += 1
                                done = 1
                                break
                        if not done:
                            current_data["inventory"].append([map[current_data["posY"] - 1][current_data["posX"]], 1])
                        
                        map[current_data["posY"] - 1][current_data["posX"]] = " "
                        done = 1
            elif data["direction"] == "DOWN":
                if (current_data["posY"] + 1) < len(game_data["map"]):
                    if game_data["map"][current_data["posY"] + 1][current_data["posX"]] != " ":
                        done = 0
                        for i in current_data["inventory"]:
                            if i[0] == map[current_data["posY"] + 1][current_data["posX"]]:
                                i[1] += 1
                                done = 1
                                break
                        if not done:
                            current_data["inventory"].append([map[current_data["posY"] - 1][current_data["posX"]], 1])
                        
                        map[current_data["posY"] + 1][current_data["posX"]] = " "
                        
                        done = 1
            elif data["direction"] == "LEFT":
                if (current_data["posX"] - 1) >= 0:
                    if game_data["map"][current_data["posY"]][current_data["posX"] - 1] != " ":
                        done = 0
                        for i in current_data["inventory"]:
                            if i[0] == map[current_data["posY"]][current_data["posX"] - 1]:
                                i[1] += 1
                                done = 1
                                break
                        if not done:
                            current_data["inventory"].append([map[current_data["posY"] - 1][current_data["posX"]], 1])
                        
                        map[current_data["posY"]][current_data["posX"] - 1] = " "
                        done = 1
            elif data["direction"] == "RIGHT":
                if (current_data["posX"] + 1) < len(game_data["map"][0]):
                    if game_data["map"][current_data["posY"]][current_data["posX"] + 1] != " ":
                        done = 0
                        for i in current_data["inventory"]:
                            if i[0] == map[current_data["posY"]][current_data["posX"] + 1]:
                                i[1] += 1
                                done = 1
                                break
                        if not done:
                            current_data["inventory"].append([map[current_data["posY"] - 1][current_data["posX"]], 1])
                        
                        map[current_data["posY"]][current_data["posX"] + 1] = " "
                        done = 1
            else:
                self.send_json({
                    "errorcode": __main__.MALFORMED_QUERY,
                    "mode": "multiplayer_map_game",
                })
                return
            game_data["map"] = ["".join(i) for i in map]

        self.factory.serv_data["multiplayer_game"] = game_data
        current_account["client_data"]["multiplayer_game"] = current_data
        self.factory.accounts[self.factory.users[self.ID]["user"]] = current_account
        
        self.send_json({
            "map": self.factory.serv_data["multiplayer_game"]["map"],
            "players": {i: self.factory.accounts[i]["client_data"].get("multiplayer_game") for i in self.factory.accounts},
            "errorcode": 0,
            "mode": "multiplayer_map_game",
            "current_account": self.factory.users[self.ID]["user"]
        })
        autosave(self)

class STATE_map_game_ACTION_init:
    mode = "multiplayer_map_game"
    action = "init"
    def run(self, data):
        current_account = self.factory.accounts[self.factory.users[self.ID]["user"]]
        game_data = self.factory.serv_data["multiplayer_game"]
        
        if not current_account["client_data"].get("multiplayer_game"):
            current_account["client_data"]["multiplayer_game"] = {
                "posX": 0,
                "posY": 0,
                "first_letter": self.factory.users[self.ID]["user"][0],
                "inventory": [],
            }

        current_account["client_data"]["multiplayer_game"]["first_letter"] = self.factory.users[self.ID]["user"][0]
        
        self.factory.serv_data["multiplayer_game"] = game_data
        self.factory.accounts[self.factory.users[self.ID]["user"]] = current_account
        
        self.send_json({
            "map": game_data["map"],
            "players": {i: self.factory.accounts[i]["client_data"].get("multiplayer_game") for i in self.factory.accounts},
            "errorcode": 0,
            "mode": "multiplayer_map_game",
            "current_account": self.factory.users[self.ID]["user"]
        })
        autosave(self)

class STATE_map_game_ACTION_pass:
    mode = "multiplayer_map_game"
    action = "pass"
    def run(self, data):
        game_data = self.factory.serv_data["multiplayer_game"]
        self.send_json({
            "map": game_data["map"],
            "players": {i: self.factory.accounts[i]["client_data"].get("multiplayer_game") for i in self.factory.accounts},
            "errorcode": 0,
            "mode": "multiplayer_map_game",
            "current_account": self.factory.users[self.ID]["user"]
        })

def autosave(self):
    global ticks_before_autosave
    ticks_before_autosave -= 1
    if ticks_before_autosave == 0:
        ticks_before_autosave = 30
        x = open("accounts.json", "w")
        json.dump(self.factory.accounts, x)
        x.close()
        
        x = open("serv_data.json", "w")
        json.dump(self.factory.serv_data, x)
        x.close()
        print("autosaved!")

functions = [STATE_map_game_ACTION_move, STATE_map_game_ACTION_init, STATE_map_game_ACTION_pass, STATE_map_game_ACTION_place, STATE_map_game_ACTION_destroy]
