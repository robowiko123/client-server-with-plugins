import login_plugin
import mode_choice_plugin
import multiplayer_map_game

plugins = [
    login_plugin,
    mode_choice_plugin,
    multiplayer_map_game
]

for i in plugins:
    i._PLUGINS = plugins
