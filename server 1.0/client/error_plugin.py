import sys
import time

UNKNOWN_ACTION         = 1
MALFORMED_QUERY        = 2
WRONG_PASSWD           = 3
ACCOUNT_ALREADY_EXISTS = 4
ACCOUNT_DOESNT_EXIST   = 5

_PLUGINS = []

class error_handler:
    mode = None
    def run(self, data):
        new_data = data
        
        errorcode = data["errorcode"]
        if errorcode == UNKNOWN_ACTION:
            print("""Unexpected error: UNKNOWN_ACTION
        This error might happen if the server is old and incompatible
        with the new client.""")
            self.conn.close()
            sys.exit(1)
        elif errorcode == MALFORMED_QUERY:
            print("Error: MALFORMED_QUERY, resending message...")
            self.conn.send(self.old_value_sent)
            new_data = self.recv_json(self.conn)
            time.sleep(.2)
        elif errorcode == WRONG_PASSWD:
            if data["mode"] == "login":
                print("Wrong password!")
            else:
                print("Error: Invalid error code. (WRONG_PASSWD outside login mode)")
                self.conn.close()
                sys.exit(1)
        elif errorcode == ACCOUNT_ALREADY_EXISTS:
            if data["mode"] == "login":
                print("Account already exists!")
            else:
                print("Error: Invalid error code. (ACCOUNT_ALREADY_EXISTS outside login mode)")
                self.conn.close()
                sys.exit(1)
        elif errorcode == ACCOUNT_DOESNT_EXIST:
            if data["mode"] == "login":
                print("Account doesn't exist!")
            else:
                print("Error: Invalid error code. (ACCOUNT_DOESNT_EXISTS outside login mode)")
                self.conn.close()
                sys.exit(1)
        
        return new_data

functions = [error_handler]
