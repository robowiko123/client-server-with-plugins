_PLUGINS = []

data = {
    "name": "account settings",
    "desc": "Manage the account",
    "SHOW_IN_LIST": 0
}

class STATE_account_settings_choose:
    mode = "account settings"
    action = "choice"
    def run(self, data):
        if data["action"] == "delete":
            self.factory.users[self.ID]["mode"] = "login"
            self.send_json({
                "mode": "login",
                "errorcode": 0
            })
            del self.factory.accounts[self.factory.users[self.ID]["user"]]
            print("account %s gone" % self.factory.users[self.ID]["user"])

functions = [STATE_account_settings_choose, ]
