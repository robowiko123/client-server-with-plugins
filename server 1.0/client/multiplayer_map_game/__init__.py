import sys
import json
import time
import multiplayer_map_game.getch_thing as con

_PLUGINS = []
this = sys.modules[__name__]

import ctypes

kernel32 = ctypes.windll.kernel32
kernel32.SetConsoleMode(kernel32.GetStdHandle(-11), 7)

OOBMAPFIX = False

class STATE_map_game:
    mode = "multiplayer_map_game"

    next_action = "init"
    lastdir = "RIGHT"
    lastindex = 0
    def run(self, data):
        global OOBMAPFIX
        # print(json.dumps(data, indent=2))
        if this.STATE_map_game.next_action == "init":
            print("\033[1J", end="")
            x = input("OOB Map fix (May result in random white blinking, and on slower computers may be unplayable) (y/n)? ")
            if x.lower() == "y":
                OOBMAPFIX = True
            self.send_json(self.conn, {
                "action": "init",
            })
            this.STATE_map_game.next_action = "move"
        else:
            print("\033[H", end="")
            
            this_player_pos = data["players"][data["current_account"]]
            
            SCREEN_SIZE_X = 20 # chars
            SCREEN_SIZE_Y = 10 # chars

            if OOBMAPFIX:
                for _ in range(SCREEN_SIZE_Y):
                    print("\033[47;1m \033[0m" * SCREEN_SIZE_X)
            
            y = max(0, this_player_pos["posY"] - int(SCREEN_SIZE_Y / 2))

            print("\033[H", end="")
            
            for row in data["map"][y:min(y + SCREEN_SIZE_Y, len(data["map"]))]:
                x = max(0, this_player_pos["posX"] - int(SCREEN_SIZE_X / 2))
                for cell in row[x:min(x + SCREEN_SIZE_X, len(data["map"][0]))]:
                    do_cont = False
                    for player_name in data["players"]:
                        if player_name != data["current_account"]:
                            if data["players"][player_name]:
                                if data["players"][player_name]["posX"] == x:
                                    if data["players"][player_name]["posY"] == y:
                                        print(data["players"][player_name].get("first_letter", "?"), end="")
                                        x += 1
                                        do_cont = True
                                        break
                    if data["players"][data["current_account"]]["posX"] == x:
                        if data["players"][data["current_account"]]["posY"] == y:
                            print("$", end="")
                            x += 1
                            continue
                    if do_cont: continue
                    if cell == " ":
                        print("\033[42m \033[0m", end="")
                    elif cell == "#":
                        print("\033[41;1m \033[0m", end="")
                    else:
                        print(cell, end="")
                    # print(cell, end="")
                    x += 1
                y += 1
                print()

            x = 0
            for i in data["players"][data["current_account"]]["inventory"]:
                print("%s*%d %s" % (i[0], i[1], ("<" if this.STATE_map_game.lastindex == x else " ")))
                x += 1
            
            if con.kbhit():
                inp = con.getwch()
                if inp == "à":
                    inp = con.getwch()
                    if inp == "H":
                        self.send_json(self.conn, {
                            "action": "move",
                            "direction": "UP"
                        })
                        this.STATE_map_game.lastdir = "UP"
                        return
                    elif inp == "P":
                        self.send_json(self.conn, {
                            "action": "move",
                            "direction": "DOWN"
                        })
                        this.STATE_map_game.lastdir = "DOWN"
                        return
                    elif inp == "M":
                        self.send_json(self.conn, {
                            "action": "move",
                            "direction": "RIGHT"
                        })
                        this.STATE_map_game.lastdir = "RIGHT"
                        return
                    elif inp == "K":
                        self.send_json(self.conn, {
                            "action": "move",
                            "direction": "LEFT"
                        })
                        this.STATE_map_game.lastdir = "LEFT"
                        return
                elif inp == "q":
                    self.send_json(self.conn, {
                        "action": "place",
                        "direction": this.STATE_map_game.lastdir,
                        "index": this.STATE_map_game.lastindex
                    })
                    return
                elif inp == "w":
                    self.send_json(self.conn, {
                        "action": "destroy",
                        "direction": this.STATE_map_game.lastdir
                    })
                    return
                elif inp == "1":
                    this.STATE_map_game.lastindex = 0

                elif inp == "2":
                    this.STATE_map_game.lastindex = 1

                elif inp == "3":
                    this.STATE_map_game.lastindex = 2

        time.sleep(.2)
        self.send_json(self.conn, {
            "action": "pass"
        })

functions = [STATE_map_game, ]
