import sys
import json
import socket
import hashlib

import plugins

UNKNOWN_ACTION         = 1
MALFORMED_QUERY        = 2
WRONG_PASSWD           = 3
ACCOUNT_ALREADY_EXISTS = 4
ACCOUNT_DOESNT_EXIST   = 5

old_value_sent = None

def send_json(conn, dict_):
    global old_value_sent
    x = json.dumps(dict_).encode("Latin-1").ljust(2048, b"\x01")
    old_value_sent = x
    return conn.send(x)

def recv_json(conn):
    return load_json(conn.recv(2048))

def load_json(raw):
    return json.loads(raw.rstrip(b"\x01").decode("Latin-1"))

# plugins.init(sys.modules[__name__])
_PLUGINS = plugins.plugins

_tmp = input("Server address? ").split(":")
_tmp[1] = int(_tmp[1])
serv_ip, serv_port = _tmp

conn = socket.create_connection((serv_ip, serv_port))

while True:
    data = recv_json(conn)
    
    if data["errorcode"]:
        new_data = plugins.HANDLE_ERRORS.run(sys.modules[__name__], data)
    
    done = False
    for plugin in _PLUGINS:
        for function in plugin.functions:
            if function.mode == data["mode"]:
                done = True
                new_data = function.run(sys.modules[__name__], data)
                break
        if done:
            break
    if not done:
        conn.close()
        print("Error: mode %s not supported" % data["mode"])
        sys.exit(1)
