import login_plugin
import error_plugin
import choice_plugin
import multiplayer_map_game

HANDLE_ERRORS = error_plugin.error_handler

plugins = [
    login_plugin,
    error_plugin,
    choice_plugin,
    multiplayer_map_game
]

for i in plugins:
    i._PLUGINS = plugins
