import json

UNKNOWN_ACTION         = 1
MALFORMED_QUERY        = 2
WRONG_PASSWD           = 3
ACCOUNT_ALREADY_EXISTS = 4
ACCOUNT_DOESNT_EXIST   = 5

_PLUGINS = []

data = {
    "name": "login",
    "desc": "Login or create a new account",
    "SHOW_IN_LIST": 0
}

class STATE_new_account:
    mode = "login"
    action = "CREATE_NEW_ACCOUNT"
    def run(self, data):
        if data["username"] in self.factory.accounts:
            self.send_json({
                "mode": "login",
                "errorcode": ACCOUNT_ALREADY_EXISTS
            })
            return
        self.factory.accounts.update({
            data["username"]: {
                "passwd": data["passwd_hashed"],
                "desc": data["desc"],
                "client_data": dict(),
            }
        })
        print(json.dumps(self.factory.accounts, indent=2))
        self.send_json({
            "mode": "login",
            "errorcode": 0
        })
        x = open("accounts.json", "w")
        json.dump(self.factory.accounts, x)
        x.close()
        
        x = open("serv_data.json", "w")
        json.dump(self.factory.serv_data, x)
        x.close()

class STATE_login:
    mode = "login"
    action = "login"
    def run(self, data):
        if not data["username"] in self.factory.accounts:
            self.send_json({
                "mode": "login",
                "errorcode": ACCOUNT_DOESNT_EXIST
            })
            return
        if data["passwd_hashed"] != self.factory.accounts[data["username"]]["passwd"]:
            print(data["passwd_hashed"], self.factory.accounts[data["username"]]["passwd"])
            self.send_json({
                "mode": "login",
                "errorcode": WRONG_PASSWD
            })
        self.send_json({
            "mode": "modechoice",
            "errorcode": 0,
            "servplugins": [i.data for i in _PLUGINS]
        })
        self.factory.users[self.ID]["mode"] = "modechoice"
        self.factory.users[self.ID]["user"] = data["username"]

functions = [STATE_new_account, STATE_login]
