import hashlib

class STATE_login:
    mode = "login"
    def run(self, data):
        conn = self.conn
        send_json = self.send_json
        
        x = None
        while x not in ["l", "n"]:
            x = input("(L)ogin or (N)ew account? ").lower()
        if x == "l":
            username = input("Username: ")
            password = input("Password: ")
            send_json(conn, {
                "action": "login",
                "username": username,
                "passwd_hashed": hashlib.sha256(password.encode("Latin-1")).hexdigest()
            })
        else:
            username = input("Username: ")
            password = input("Password: ")
            desc = input("Account desctription: ")
            send_json(conn, {
                "action": "CREATE_NEW_ACCOUNT",
                "username": username,
                "passwd_hashed": hashlib.sha256(password.encode("Latin-1")).hexdigest(),
                "desc": desc
            })
        
        return data

functions = [STATE_login, ]
