_PLUGINS = []

class STATE_choose_plugins:
    mode = "modechoice"
    def run(self, data):
        print("Choose mode:")
        for stuff in data["servplugins"]:
            if stuff["SHOW_IN_LIST"]:
                print("  %s:" % stuff["name"])
                print("    %s" % stuff["desc"])
        x = None
        while x not in [i["name"] for i in data["servplugins"]]:
            x = input("> ")
        self.send_json(self.conn, {
            "action": "mode_choice",
            "choice": x,
        })

functions = [STATE_choose_plugins,]
